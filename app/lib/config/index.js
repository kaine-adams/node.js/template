var config = {};

config.bcrypt = {
    factor: parseInt(process.env.BCRYPT_FACTOR) || 10,
}

config.passport = {
    session_secret: process.env.SESSION_SECRET || 'keyboard cat',
}

config.server = {
    port: process.env.NODE_PORT || 3000,
};

config.database = {};

config.database.mongodb = {
    uri: process.env.MONGODB_URI || 'mongodb://localhost/test',
};
config.database.session = {
    uri: process.env.MONGODB_URI || 'mongodb://localhost/test',
};

module.exports = config;