const passport = require('passport');
const bcrypt = require('bcrypt');
const LocalStrategy = require('passport-local').Strategy;
const passportSocketIo = require('passport.socketio');
const cookieParser = require('cookie-parser');
const session = require('express-session');

//database
const MongoStore = require('connect-mongo')(session);

//local config module
const config = require('config');

//local user handler:
const users = require('database/users');

function getSessionStore() {
    let db = require('mongodb').get;
    return new MongoStore({ url: config.database.session.uri });
}

function requireAuthentication(options) {
    return require('connect-ensure-login').ensureLoggedIn(options);
}

function initializePassport(options) {

    passport.serializeUser(function (user, callback) {
        callback(null, user.id);
    });

    passport.deserializeUser(function (id, callback) {
        users.findById(id, function (err, user) {
            if (err) { return callback(err); }
            callback(null, user);
        });
    });

    passport.use(new LocalStrategy(
        function (username, password, callback) {
            users.findByUsername(username, function (err, user) {
                if (err) { return callback(err); }
                if (!user) { return callback(null, false); }
                if (user.password != password) { return callback(null, false); }
                return callback(null, user);
            });
        }));

    let sessionStore = getSessionStore();

    options.app.use(require('body-parser').urlencoded({ extended: true }));

    options.app.use(session({
        secret: config.passport.session_secret,
        resave: false,
        saveUninitialized: false,
        store: sessionStore
    }));

    options.app.use(passport.initialize());
    options.app.use(passport.session());

    options.io.use(passportSocketIo.authorize({
        key: 'connect.sid',
        secret: config.passport.session_secret,
        passport: passport,
        cookieParser: cookieParser,
        store: sessionStore
    }));

    passport.requireAuthenticationMiddleware = requireAuthentication;
}

module.exports = initializePassport;