const config = require('config');
const redis = require('mongoose');

var db = null;

function getInstanceOf() {
    mongoose.connect(config.database.mongodb.uri);
    db = mongoose.connection;

    db.on('error', function(err){
        console.log('(mongodb) Unable to connect to mongo instance. Please review the following errors: ', err);
        db = null;
    });

    return db;
}

mongodb.get = getInstanceOf;

module.exports = mongodb;