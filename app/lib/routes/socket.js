const shortid = require('shortid');


module.exports = function(io) {
    io.on('connection', function(socket) {
        
        socket.id = shortid.generate();
        
        console.log("Client connected: ", socket.id);

        socket.on('disconnect', function(){
            console.log("Client disconnected: ", socket.id);
        });
    });
}