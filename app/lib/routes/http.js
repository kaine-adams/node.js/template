let express = require('express');
let passport = require('passport');
let csrf = require('csurf');

module.exports = function(app) {
    let csrfProtection = csrf();

    //authentication mount points:
    app.get('/authentication/login',
        csrfProtection,
        function(req, res){
            res.render('authentication/login', { csrfToken: req.csrfToken() });
    });

    app.post('/authentication/login', 
        csrfProtection,
        passport.authenticate('local', { failureRedirect: '/authentication/login' }),
        function(req, res) {
            res.redirect('/');
    });

    app.get('/authentication/logout',
        function(req, res){
            req.logout();
            res.redirect('/');
    });

    //public mount points
    app.get('/',
        function(req, res) {
            res.render('home', { user: req.user });
    });
    
    //authenticated mount points
    app.use(passport.requireAuthenticationMiddleware({ redirectTo: '/authentication/login'}));

    app.get('/profile',
    function(req, res){
        res.render('profile', { user: req.user });
    })
}