var game = {
    connection: null,
    action: {
        update: function(action) {
            game.connection.emit('action.update',{action: action});
        }
    },
    init: function() {
        game.connection = io();

        game.connection.on("resource.update", function(data) {
            $('#wood-resource')[0].innerText = data.wood;
            $('#ore-resource')[0].innerText = data.ore;
            $('#wheat-resource')[0].innerText = data.wheat;
        });
    }
}

game.init();